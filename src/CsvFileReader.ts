import fs from "fs";

export class CsvFileReader {
  data: string[][] = [];

  constructor(public filename: string) {}

  // Transform the csv into an array of arrays containing strings
  // and make sure each item in the array of strings is the correct data structure
  read(): void {
    this.data = fs
      .readFileSync(this.filename, {
        encoding: "utf-8"
      })
      .split("\n")
      .map((row: string): string[] => row.split(","));
  }
}
