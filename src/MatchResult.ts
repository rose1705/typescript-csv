// enum - enumeration
// an object that stores some closely related values
// purpose: signal that this is a collection of closely related values
export enum MatchResult {
  HomeWin = "H",
  AwayWin = "A",
  Draw = "D"
}
