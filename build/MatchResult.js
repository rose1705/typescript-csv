"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// enum - enumeration
// an object that stores some closely related values
// purpose: signal that this is a collection of closely related values
var MatchResult;
(function (MatchResult) {
    MatchResult["HomeWin"] = "H";
    MatchResult["AwayWin"] = "A";
    MatchResult["Draw"] = "D";
})(MatchResult = exports.MatchResult || (exports.MatchResult = {}));
